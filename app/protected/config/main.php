<?php

return [
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Whois',
	'preload'=>['log'],
	'import'=>array(
		'application.components.*',
		'application.models.*',
	),
	'components'=>[
		'db'=>[
			'connectionString' => 'sqlite:protected/data/domains.db',
		],
//		'errorHandler'=>array(
//			'errorAction'=>'site/error',
//		),
		'log'=>[
			'class'=>'CLogRouter',
			'routes'=>[
				[
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				],
			],
		],
	],
	'params'=>require(dirname(__FILE__).'/params.php'),
];