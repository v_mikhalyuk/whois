<?php

/**
 * Главный контроллер.
 */
class SiteController extends CController
{

	/**
	 * Главный экшн.
	 */
	public function actionIndex()
	{
		$model = new DomainInputForm();
		$domainModel = new Domain();
		$dataProvider = new CActiveDataProvider('Domain', [
			'pagination' => [
				'pageSize' => 2
			],
			'sort' => [
				'defaultOrder' => [
					'created_at' => CSort::SORT_DESC,
				]
			]
		]);
		if(isset($_POST['ajax']) && $_POST['ajax']==='domain-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
		if(isset($_POST['DomainInputForm'])) {
			$model->attributes = $_POST['DomainInputForm'];
			if ($model->validate()) {
				$whois = new Whois();
				$info = $whois->query($model->domain);
				if ($info !== false) {
					$domainModel->domain = $model->domain;
					$domainModel->info = $info;
					$domainModel->save();
				}
			};
		}
		$this->render('index', ['model' => $model, 'domain' => $domainModel, 'dataProvider' => $dataProvider]);
	}

	/**
	 * Удаление домена.
	 *
	 * @param integer $id id домена
	 */
	public function actionDelete($id)
	{
		$domain = Domain::model()->findByPk($id);
		$domain->delete();
		if(!isset($_POST['ajax'])) {
			$this->redirect(['index']);
		}
	}

}