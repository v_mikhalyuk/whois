<?php

/**
 * Поля в таблице 'domains':
 * @property integer $id
 * @property string $domain
 * @property string $info
 * @property integer $created_at
 * @property integer $updated_at
 */
class Domain extends CActiveRecord
{

    /**
     * @inheritdoc
     */
    public function tableName()
    {
        return 'domains';
    }

    /**
     * @inheritdoc
     */
    public function behaviors(){
        return [
            'CTimestampBehavior' => [
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'created_at',
                'updateAttribute' => 'updated_at',
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['domain, info', 'required'],
            ['domain', 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

}