<?php


/**
 * Модель формы ввода домена.
 */
class DomainInputForm extends CFormModel
{

    /**
     * @var string домен
     */
    public $domain;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['domain', 'required'],
            ['domain', 'checkDomain']
        ];
    }

    public function checkDomain($attribute)
    {
        //$regEx = '/^[a-zA-Z0-9][a-zA-Z0-9-]{1,61}[a-zA-Z0-9]\.[a-zA-Z]{2,}$/';
        $regEx = '/^([-a-z0-9]{2,100})\.([a-z\.]{2,8})$/i';
        if (!preg_match($regEx, $this->$attribute)) {
            $this->addError($attribute, 'Wrong domain name.');
        }
    }

}