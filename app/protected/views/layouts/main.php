<?php

/** @var $content string */
/** @var $this Controller */

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?= $this->pageTitle ?></title>
</head>
<body>
<?= $content ?>
</body>
</html>