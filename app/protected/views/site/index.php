<?php

/** @var $model DomainInputForm */
/** @var $this SiteController */

?>

<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'domain-form',
        'enableAjaxValidation' => true,
    )); ?>

    <div class="row">
        <?= $form->labelEx($model, 'domain'); ?>
        <?= $form->textField($model, 'domain'); ?>
        <?= $form->error($model, 'domain'); ?>
    </div>

    <div class="row submit">
        <?= CHtml::submitButton('Submit'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div>

<?php if (isset($domain)) : ?>
    <?= CHtml::errorSummary($domain) ?>
<?php endif; ?>

<?php if (isset($dataProvider)) : ?>
    <?php $this->widget('zii.widgets.grid.CGridView', [
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'domain',
            'info:html',
            [
                'name'=>'created_at',
                'value'=>'date("M j, Y", $data->created_at)',
            ],
            [
                'class'=>'CButtonColumn',
                'template' => '{delete}'
            ],
        ],
    ]); ?>
<?php endif; ?>
